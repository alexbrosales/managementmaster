﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TMaye_Movement : MonoBehaviour {

    // Developer Name: Trevor Maye
    // Contribution: Creation of clicking and dragging to move cards around the table
    // Feature: Moving Cards
    // Start & End dates: 10/4/2017-10/5/2017
    // References: N/A
    // Links: N/A

	// Developer Name: Alex Rosales
	// Contribution: Adding locked function to allow locking of cards and restricting movement when cards are locked
	// Feature: Locked
	// Start & End dates: 11/4/2017-11/4/2017
	// References: N/A
	// Links: N/A

    private Vector3 Spoint;
    private Vector3 set;

	public bool Locked = false;

	// Use this for initialization
	void Start () {
        transform.parent = null;
		
	}
	
	// Update is called once per frame
	void Update () {
            
	}

    void OnMouseDown()
    {
		//Only execute if the card is not locked
		if (Locked == false) {
			Spoint = Camera.main.WorldToScreenPoint (transform.position);
			set = transform.position - Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, Spoint.z));
		}
    }

    void OnMouseDrag()
    {
		//Only execute if the card is not locked
		if (Locked == false) {
			Vector3 curSPoint = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, Spoint.z);
			Vector3 curPosition = Camera.main.ScreenToWorldPoint (curSPoint) + set;
			transform.position = curPosition + new Vector3 (0, 1.5f, 0);
		}
    }
	//Toggle lock
	void OnMouseOver()
	{
		if(Input.GetKeyUp (KeyCode.L))
		{
			if(Locked == true)
			{
				Locked = false;
				if(transform.childCount > 0){
					Destroy (this.transform.GetChild (2).gameObject);
				}
			}
			else if(Locked == false)
			{
				Locked = true;
				GameObject Cube = GameObject.CreatePrimitive (PrimitiveType.Cube);
				Cube.transform.position = this.transform.position;
				Cube.transform.localScale = new Vector3 (1.0f, 0.1f, 1.0f);
				Cube.transform.parent = this.gameObject.transform;
			}
		}
	}
}