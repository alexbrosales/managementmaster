﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class DL_StartScene : MonoBehaviour {
	public string GameModeScene;
	public string FreeModeScene;
	// Use this for initialization
	/*Developer Name: Dung Le

	        Contribution: Start scene in the game

	               Feature

	                Start & End dates 11/9 - 11/10/2017

	               References:
	                      Links:*/
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void LoadSceneGameMode()
	{
		SceneManager.LoadScene (GameModeScene);

	}

	public void LoadSceneFreeMode()
	{
		
		SceneManager.LoadScene (FreeModeScene);
	}
	public void Quit()
	{

		Application.Quit ();
	}


}
