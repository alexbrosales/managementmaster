﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
public class BlackJackScript : MonoBehaviour {
	//Developer's name : Dung Le
	//Contribution : BlackJackScript
	//Feature: All the UI related to blackjack customizable feature
	// Use this for initialization
	public enum Opertator{ larger, smaller, equal}
	public Opertator currentOperator;
	public Text SignSymbol;
	public InputField CurrentField;
	UISystem UIAccess;
	void Start () {
		UIAccess = GameObject.FindObjectOfType<UISystem> ();
		UIAccess.WinningScore = 21; 
	}
	
	// Update is called once per frame
	public void SetOperator()
	{
		switch (currentOperator)
		{
		case Opertator.larger:
			SignSymbol.text = "smaller than ";
			currentOperator = Opertator.smaller;
				break;
		case Opertator.equal:
			SignSymbol.text = "larger than ";
			currentOperator = Opertator.larger;
			break;
		case Opertator.smaller:
			SignSymbol.text = "equal to ";
			currentOperator = Opertator.equal;
			break;

		}
	}
	public void ApplyWinningScore()
	{
		try
		{
			int i =Int32.Parse (CurrentField.text);
			/*int.TryParse (CurrentField.text, out i);*/
		UIAccess.WinningScore = i;
		}
		catch
		{
			print ("Error");
		}
	}
}
