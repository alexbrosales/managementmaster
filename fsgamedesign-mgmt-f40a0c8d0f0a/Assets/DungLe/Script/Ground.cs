﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour {
	//Developer's name: Dung Le
	// Contribution: Ground Script to prevent card from falling out;
	// Use this for initialization
	void OnCollisionStay(Collision other)
	{

		if (other.gameObject.GetComponent <CardClass> ()!= null) {

			other.gameObject.GetComponent <CardClass> ().RestartState ();
		}
		if (other.gameObject.GetComponent <PileParent> ()!= null) {

			other.transform.position = new Vector3 (0, 10, 0);
		}

	}
}
