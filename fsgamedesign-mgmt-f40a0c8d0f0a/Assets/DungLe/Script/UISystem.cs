﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UISystem : MonoBehaviour {
	/*Developer Name: Dung Le

	        Contribution: Run Time UIScript

	               Feature

	               Start & End dates
Start & End dates: 11/4/2017 - 11/10/2017
	               References:
	                      Links:*/
	public Text Player1Info;
	public Text Player2Info;
	public Text WhoWin;
	public Text TimerText;
	public AR_Timer TimerAccess;
	private float Timer;
	public GameSystem GS;
	public Text GameModeText;
	public int WinningScore = 21;
	[SerializeField]
	private InputField Email1;
	[SerializeField]
	private GameObject Manual;
	[SerializeField]
	private GameObject Manual2;


	// Use this for initialization
	void Start () {
		GS = GameObject.FindObjectOfType<GameSystem> ();
		TimerAccess = GameObject.FindObjectOfType<AR_Timer> ();
		GameModeText.text = "Play";
	}
	
	// Update is called once per frame
	void Update () {
		GetPoint ();

		if(TimerAccess.TimerActive)
		{
			if (TimerText.enabled == false)
				TimerText.enabled = true;
			
			Timer += Time.deltaTime;
			TimerText.text = "Time: " + Timer.ToString ("F0");
		} else {
			TimerText.enabled = false;
			Timer = 0.0f;
		}
	}
	public void UpdateEmail()
	{
		GS.CurrentPlayer.PCemail = Email1.text;
	}
	public void SaveCardPos ()
	{
		CardClass[] AllCards = GameObject.FindObjectsOfType <CardClass> ();
		foreach(var g in AllCards)
		{
			g.SavePosition ();
		}
	}
	public void LoadCardPos()
	{
		CardClass[] AllCards = GameObject.FindObjectsOfType <CardClass> ();
		foreach(var g in AllCards)
		{
			g.LoadOldPosition ();
		}
	}


	public void GetPoint()
	{
		
		//Player1Info.text = GS.PC1.points.ToString ();

		//Player2Info.text = GS.PC2.points.ToString ();

		if (GS.CurrentGamestate == GameSystem.State.GameDone) {
			switch (GS.CurrentGameMode) {


			case GameSystem.GameMode.BlackJack:
				int maxscore = int.MinValue;
				int thewinner = 0;
				List <int> PlayerScore = new List <int> (GS.AllPlayer.Count);
				for (int i = 0; i < GS.AllPlayer.Count; i++) {
					PlayerScore.Add ((GS.AllPlayer [i].points-WinningScore));
				}
				foreach ( var g in PlayerScore)
				{
					if (g > maxscore)
					{
						maxscore = g;
						thewinner = PlayerScore.IndexOf (g);
					}
				}

				WhoWin.text ="Player " + (thewinner+1).ToString () + " win!";
				GS.AllPlayer[thewinner].MS.SendSMS (GS.AllPlayer[thewinner].PhoneNumber,"Hey you are the winner!");
				break;


			}
		}
		else

		{
			WhoWin.text = "";
		}
	}
	public void GameModeSwitch()
	{
		if(GS.CurrentGameMode == GameSystem.GameMode.Freemode)
		{
			GS.CurrentGameMode = GameSystem.GameMode.Solitare;
			GameModeText.text = "Solitare";
		}
		else if(GS.CurrentGameMode == GameSystem.GameMode.BlackJack)
		{
			GS.CurrentGameMode = GameSystem.GameMode.Freemode;
			GameModeText.text = "Free Mode";
		}
		else if (GS.CurrentGameMode == GameSystem.GameMode.Solitare)
		{
			GS.CurrentGameMode = GameSystem.GameMode.BlackJack;
			GameModeText.text = "Black Jack";
		}
	}
	public void FlippCards()
	{
		GS.FlippedAllCards ();
	}
	public void OpenManual()
	{
		if (Manual.activeSelf != true) {
			Manual.SetActive (true);
		}
		else
		{
			Manual.SetActive (false);
		}

	}
	public void ManualPage2(bool onornoff)
	{
		Manual2.SetActive (onornoff);
	}
}
