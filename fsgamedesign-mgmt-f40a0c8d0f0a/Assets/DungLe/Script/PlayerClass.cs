﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rover{
public class PlayerClass : MonoBehaviour {
	//Developer Name: Dung Le

	//         Contribution: Player Class in the game

	//                Feature

		//                Start & End dates Start & End dates: 11/4/2017 - 11/10/2017

	//                References:

	//                        Links:
	// Use this for initialization
	public enum Phase{Draw,Fold,Bet,End};
	public Phase CurrentPhase;
	public GameSystem.State team;
		public string PhoneNumber;

	public int points; 
	public List <CardClass> CardList;
	public bool AllFull;
	private GameSystem GS;
		public string PCemail;
		public MailSystem MS;
	void Start () {
		GS = GameObject.FindObjectOfType<GameSystem> ();
			MS = GameObject.FindObjectOfType<MailSystem> ();
			//StartCoroutine (MS.SendMailV2 (PCemail,"Card Managaement","Welcome to the Card Manage game"));


	}
	
	// Update is called once per frame
	void Update () {

		if (CardList.Count > 0) {
			for (int i = 0; i < CardList.Count ; i++) {

				if (CardList [i] == null) {
					points = points - CardList [i].pointvalue;
					CardList.RemoveAt (i);

					print ("Hi");
				}
			}
		}
		



		}
	public int SumPoints()
	{
		for (int i = 0; i < CardList.Count; i ++)
		{
				points = points + CardList[i].pointvalue ;
		}
		return points;
	}


	public void SwitchOnGameMode()
	{
		switch (GS.CurrentGameMode)
		{

			case GameSystem.GameMode.BlackJack:


				//Switch on the phase in blackjack mode;
				switch (CurrentPhase) {
				case Phase.End:
					if (GS.CurrentPlayer == GS.AllPlayer [GS.AllPlayer.Count - 1]) {
						GS.CurrentPlayerIndex = 0;
						GS.CurrentPlayer = GS.AllPlayer [GS.CurrentPlayerIndex];


					} else {
						GS.CurrentPlayerIndex = GS.CurrentPlayerIndex + 1;
						GS.CurrentPlayer = GS.AllPlayer [GS.CurrentPlayerIndex];

					}
					AllFull = true;
					break;
				}
			break;

		
	}
		}
	void OnTriggerEnter(Collider other)
	{
		
		if(other.GetComponent <CardClass>()!= null)
		{
			CardClass otherAccess = other.GetComponent <CardClass> ();
			


				switch (GS.CurrentGameMode) {

				case GameSystem.GameMode.BlackJack: //if the game mode is black jack
					if (AllFull != true) { //If a player is not full ( number of card not more than 5)

						CardList.Add (otherAccess);

						if (otherAccess.CardValue == CardClass.Value.one) {

							if ((otherAccess.pointvalue + points) > 21) {
								points = 1 + points;
							} else {
								points = otherAccess.pointvalue + points;
							}
						} 
						else {
							points = otherAccess.pointvalue + points;
						}



							StartCoroutine (MS.SendMailV2 (PCemail, "Card Receive", "You just received a " + otherAccess.CardValue.ToString () + otherAccess.CardType.ToString ()));
							print (PCemail + "Card Receive" + "You just received a " + otherAccess.CardValue.ToString () + otherAccess.CardType.ToString ());

						//MS.SendSMS ("4074842826","Player " + team.ToString ()+ " just received " + otherAccess.CardValue.ToString () + otherAccess.CardType.ToString ());

							//MS.SendSMS (PhoneNumber, "You just received a " + otherAccess.CardValue.ToString () + otherAccess.CardType.ToString ());

					}
						break;
					
				}
			}

		}
			

	
	void OnTriggerExit(Collider other)
	{

		if(other.GetComponent <CardClass>()!= null)
		{
			CardClass otherAccess = other.GetComponent <CardClass> ();
			CardList.Remove (otherAccess);
			points = points - otherAccess.pointvalue;
		}
	}
}
}