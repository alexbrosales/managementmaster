﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardClass : MonoBehaviour {
	/*Developer Name: Dung Le

	//         Contribution: Card Class for each card game object

	//                Feature

	//                Start & End dates

	//                References:

	                       Links:*/
	public enum Type{Spade,Diamond,Club,Heart};
	public enum Value{one,two,three,four,five,six,seven,eight,nine,ten,jack,queen,king};
	public enum CardColor {Black,Red};
	private GameSystem GS;
	private DeckSpawn DeckAcess;
	public GameSystem.State CurrentState;

	public int CardOrder;//This is the index of the card when being drawn from a deck
	public Type CardType;// This is the type of the card: Spade,Diamond,Club,Heart
	public Value CardValue;// this is the value of the card
	public CardColor C_COLOR;

	DL_SolitareSLot SL_Access;
	public int pointvalue;// This is the value points of the cards
	public int BlackJackPoints;// This is the value points of the card when in the black jack mode.
	private Vector3 CurrentPos;
	private Vector3 SavePos;

	// Use this for initialization
	void Start () {
		GS = GameObject.FindObjectOfType<GameSystem> ();
		//DeckAcess = GameObject.FindObjectOfType<DL_Deck> ();
		//SL_Access = GetComponent <DL_SolitareSLot> ();
		DeckAcess = GameObject.FindObjectOfType <DeckSpawn> ();
		CurrentPos = transform.position;
		SavePos = CurrentPos;
	}
	
	// Update is called once per frame
	void Update () {
		CurrentState = GS.CurrentGamestate;
		pointvalue = Mathf.Clamp (pointvalue, 0, 13);
		SwitchOnGameMode ();

	}

	void SwitchOnGameMode()
	{
		switch (GS.CurrentGameMode)
		{

		case GameSystem.GameMode.BlackJack:
			pointvalue = BlackJackPoints;
//			SL_Access.enabled = false;
			break;

		case GameSystem.GameMode.Solitare:
			 
			//SL_Access.enabled = true;
			break;

		}



	}
	public Vector3 GetCurrentPos()
	{
		return CurrentPos;
	}
	void OnMouseDown()
	{
		CurrentPos = transform.position;
	}

	public void RestartState()
	{
		//DeckAcess.CardIndex.Add (CardOrder);
		transform.position = DeckAcess.transform.position;
		transform.rotation = Quaternion.Euler (0, 0, 90);
		GetComponent <Rigidbody>().velocity = Vector3.zero;
	}
	public void SavePosition()
	{
		//DeckAcess.CardIndex.Add (CardOrder);
		SavePos = transform.position;
	}
	public void LoadOldPosition()
	{
		//DeckAcess.CardIndex.Add (CardOrder);
		transform.position = SavePos;
		GetComponent <Rigidbody>().velocity = Vector3.zero;
	}
}
