﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rover;
using SolitareMode;
public class GameSystem : MonoBehaviour {
	/*Developer Name: Dung Le

	        Contribution: Game System for card

	               Feature

	             Start & End dates: 11/4/2017 - 11/10/2017

	               References:
	                      Links:*/
	// Use this for initialization

	// This script is used for when the player needs an actual ref to play certain game card like blackjack; 
	// This script contains the game stage and all most every script will access to this script: 
	public enum GameMode {BlackJack,Solitare,Freemode};

	public enum State{P1,P2,P3,P4,Reset,GameDone};
	public enum NumberofPlayer{ one , two ,three , four};

	public GameMode CurrentGameMode;
	public State CurrentGamestate;
	public int CurrentPlayerIndex;
	public PlayerClass CurrentPlayer;





	public NumberofPlayer numberofplayer;


	public GameObject [] AllGameMode;

	public List<PlayerClass> AllPlayer;

	void Start () {
		CurrentPlayerIndex = 0;
		CurrentPlayer = AllPlayer [CurrentPlayerIndex];
		CurrentGameMode = GameMode.Freemode;
	}
	
	// Update is called once per frame
	void Update () {
		SwitchState ();
		InstantiatePlayer ();
		SwitchonGameModeAction ();


		//CurrentPlayer = AllPlayer [CurrentPlayerIndex];
	}
	public void Set1player()
	{
		numberofplayer = NumberofPlayer.one;
	}
	public void Set2player()
	{
		numberofplayer = NumberofPlayer.two;
	}
	public void Set3player()
	{
		numberofplayer = NumberofPlayer.three;
	}
	public void Set4player()
	{
		numberofplayer = NumberofPlayer.four;
	}




	public void Restart()
	{
		CurrentGamestate = State.Reset;

	}

	public void SetBlackJack()
	{
		CurrentGameMode = GameMode.BlackJack;
	}
	public void SetSolitareMode()
	{
		CurrentGameMode = GameMode.Solitare;
	}
	public void SetFreeMode()
	{
		CurrentGameMode = GameMode.Freemode;
	}
	public void SetEndState()
	{
		CurrentPlayer.CurrentPhase = PlayerClass.Phase.End;
		CurrentPlayer.SwitchOnGameMode ();
	}

	public void InstantiatePlayer()
	{
		
		if (CurrentGameMode == GameMode.BlackJack) {
			switch (numberofplayer) {
			case NumberofPlayer.one:
				AllPlayer [0].gameObject.SetActive (true);
				AllPlayer [1].gameObject.SetActive (false);
				AllPlayer [2].gameObject.SetActive (false);
				AllPlayer [3].gameObject.SetActive (false);
				if (AllPlayer [0].AllFull) {
					CurrentGamestate = State.GameDone;
				}
				break;


			case NumberofPlayer.two:

				AllPlayer [0].gameObject.SetActive (true);
				AllPlayer [1].gameObject.SetActive (true);
				AllPlayer [2].gameObject.SetActive (false);
				AllPlayer [3].gameObject.SetActive (false);


				if (AllPlayer [0].AllFull == true && AllPlayer [1].AllFull == true) {
					CurrentGamestate = State.GameDone;
				}

				break;
	
			case NumberofPlayer.three:
				AllPlayer [0].gameObject.SetActive (true);
				AllPlayer [1].gameObject.SetActive (true);
				AllPlayer [2].gameObject.SetActive (true);
				AllPlayer [3].gameObject.SetActive (false);
				if (AllPlayer [0].AllFull == true && AllPlayer [1].AllFull == true && AllPlayer [2].AllFull == true) {
					CurrentGamestate = State.GameDone;
				}

				break;
		
			case NumberofPlayer.four:
				AllPlayer [0].gameObject.SetActive (true);
				AllPlayer [1].gameObject.SetActive (true);
				AllPlayer [2].gameObject.SetActive (true);
				AllPlayer [3].gameObject.SetActive (true);
				if (AllPlayer [0].AllFull == true && AllPlayer [1].AllFull == true && AllPlayer [2].AllFull == true && AllPlayer [3].AllFull == true) {
					CurrentGamestate = State.GameDone;
				}
				break;
			}
		}
	}

	public void SwitchonGameModeAction()
	{
		switch (CurrentGameMode)
		{
		case GameMode.BlackJack:
			AllGameMode [0].SetActive (true);
			AllGameMode [1].SetActive (false);
			AllGameMode [2].SetActive (false);
			break;

		case GameMode.Solitare:
			AllGameMode [0].SetActive (false);
			AllGameMode [1].SetActive (true);
			AllGameMode [2].SetActive (false);
			break;

		case GameMode.Freemode:
			AllGameMode[0].SetActive (false);
			AllGameMode [1].SetActive (false);
			AllGameMode [2].SetActive (true);
			break;
		}
	}

	public void SwitchState()
	{

		switch(CurrentGameMode)
		{
		case GameMode.BlackJack:
			switch (CurrentGamestate) {


			case State.P1:
				break;


		
			case State.P2:
				break;


	
			case State.P3:
				break;


			case State.P4:
				break;



			case State.Reset:
				CardClass[] card = GameObject.FindObjectsOfType <CardClass> ();

				foreach (var g in card) {
					//Destroy (g.gameObject);
					g.RestartState ();
				}

				foreach (var z in AllPlayer) {
					z.AllFull = false;
				}

				CurrentGamestate = State.P1;
				break;


			}
			break;


		case GameMode.Solitare:

			switch (CurrentGamestate) {


			case State.GameDone:
				
				break;



			case State.Reset:
				
				StartCoroutine (SolitareReset ());
				CurrentGamestate = State.P1;
				break;
			}
			break;
		case GameMode.Freemode:

			switch (CurrentGamestate) {


			case State.GameDone:

				break;



			case State.Reset:

				StartCoroutine (SolitareReset ());
				CurrentGamestate = State.P1;
				break;
			}
			break;
		
	}
}
	public IEnumerator SolitareReset()
	{
		
		CardClass[] card = GameObject.FindObjectsOfType <CardClass> ();

		foreach (var g in card) {
			//Destroy (g.gameObject);
			g.RestartState ();
		}
		yield return new WaitForSeconds (2.0f);

		//DL_SolitarePile[] AllPiles = GameObject.FindObjectsOfType < DL_SolitarePile> ();
		///foreach (var z in AllPiles) {
		//	z.SpawnCard ();
		//}
	}
	public void SolitareDraw()
	{
		DL_SolitarePile[] AllPiles = GameObject.FindObjectsOfType < DL_SolitarePile> ();
		foreach (var z in AllPiles) {
			z.SpawnCard ();

		}
	}
	public void FlippedAllCards()
	{
		CardClass[] card = GameObject.FindObjectsOfType <CardClass> ();

		foreach (var g in card) {
			//Destroy (g.gameObject);
			g.transform.Rotate (0,180,0);
		}
	}
}