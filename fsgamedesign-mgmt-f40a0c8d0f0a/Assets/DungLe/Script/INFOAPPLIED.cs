﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Rover;
public class INFOAPPLIED : MonoBehaviour {
	//Developer's name: Dung Le
	//Start & End dates: 11/4/2017 - 11/10/2017
	//Info application in blackjack mode
	//This script is for obtaining the email or sms from the player


	public InputField Field_C;
	public Button SwitchButton;
	public Button ApplyButton;
	public PlayerClass Owner;
	public int Team;
	// Use this for initialization
	void Start () {
		ApplyButton.GetComponentInChildren <Text>().text = "Apply Email";
		SwitchButton.GetComponentInChildren <Text>().text = "Use Email";

		Owner = GetComponent <PlayerClass> ();
	}
	
	// Update is called once per frame
	void Update () {
		//SwitchOnText ();
	}


	public void AppliedInfo()
	{
		
			Owner.PCemail = Field_C.text.ToString ();



			StartCoroutine ( Owner.MS.SendMailV2 (Owner.PCemail,"Card Management","Welcome to Card Manage Game!\nYou are player "+ Team.ToString ()));
			ApplyButton.gameObject.SetActive (false);
			Field_C.gameObject.SetActive (false);



	}

	public void SwitchonConnection()
	{
		if (ApplyButton.gameObject.activeSelf == false) {
		
			ApplyButton.gameObject.SetActive (true);
			Field_C.gameObject.SetActive (true);

		}
		else
		{
			ApplyButton.gameObject.SetActive (false);
			Field_C.gameObject.SetActive (false);
		}

	}
}
