﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DL_SolitareSLot : MonoBehaviour {
	CardClass CardAccess;
	public bool FindNewSlot;
	public Vector3 CurrentPosition;
	public bool Flipped;
	/*Developer Name: Dung Le

	        Contribution: Solitare feature for each card in the solitare mode

	               Feature

	               Start & End dates: 11/8/2017 - 11/10/2017

	               References:
	                      Links:*/
	// Use this for initialization
	void Start () {
		CardAccess = GetComponent <CardClass> ();

		CurrentPosition = transform.position;
		Flipped = true;
	}
	void Update()
	{
		if (Flipped == true) {
			transform.rotation = Quaternion.Euler (0, 0, 90);
		}
		else
		{
			transform.rotation = Quaternion.Euler (180, 0, 90);
		}

	}

	void OnCollisionEnter( Collision other)
	{
		if (FindNewSlot == true) {
			if (other.gameObject.GetComponent <CardClass> () != null) 
			
			{

				CardClass Other_C = other.gameObject.GetComponent <CardClass> ();

				if (Other_C.C_COLOR != CardAccess.C_COLOR) {

					if (Flipped == true) {
						if (Other_C.pointvalue == CardAccess.pointvalue - 1) {
						
							if (other.gameObject.GetComponent <DL_SolitareSLot> ().Flipped == true) {
								other.transform.parent = transform;
							}
						} else if (Other_C.pointvalue == CardAccess.pointvalue + 1) {
							if (other.gameObject.GetComponent <DL_SolitareSLot> ().Flipped == true) {
								transform.parent = other.transform;

							}

						} 
						else 
						{
							transform.position = CurrentPosition;
						}
					}


				} else {
				
					transform.position = CurrentPosition;

				}


			}

		}
	}
	void OnMouseDown ()
	{
		CurrentPosition = transform.position;
		Rigidbody rB = GetComponent <Rigidbody> ();

	}
	void OnCollisionExit( Collision other)
	{
		if(other.gameObject.GetComponent <CardClass>()!= null)
		{

			CardClass Other_C = other.gameObject.GetComponent <CardClass> ();

			if(Other_C.C_COLOR != CardAccess.C_COLOR)
			{
				if(Other_C.pointvalue == CardAccess.pointvalue +1)
				{
					transform.parent = null;

				}



			}

		}

	}

}
