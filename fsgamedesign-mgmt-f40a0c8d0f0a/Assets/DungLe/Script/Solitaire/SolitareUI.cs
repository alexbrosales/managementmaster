﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SolitareUI : MonoBehaviour {
	public Sprite Clubs;
	public Sprite Spades;
	public Sprite Heart;
	public Sprite Diamond;
	public Button ChangeTypeButton;
	public Text RequiredValue;
	public SL_WinningCondition W_Pile;
	// Use this for initialization
	void Start () {
		switch(W_Pile.RequiredType)
		{
		case CardClass.Type.Club:
			ChangeTypeButton.GetComponent <Image> ().sprite = Clubs;
			break;
		case CardClass.Type.Spade:
			ChangeTypeButton.GetComponent <Image> ().sprite = Spades;
			break;
		case CardClass.Type.Diamond:
			ChangeTypeButton.GetComponent <Image> ().sprite = Diamond;
			break;

		case CardClass.Type.Heart:
			ChangeTypeButton.GetComponent <Image> ().sprite = Heart;
			break;
		}
		RequiredValue.text = "Ace";
	}
	public void SwitchValue()
	{
		
		switch (W_Pile.RequiredValue) 

		{
		case CardClass.Value.one:
			W_Pile.RequiredValue = CardClass.Value.two;
			RequiredValue.text = "2";
			break; 
		case CardClass.Value.two:
			W_Pile.RequiredValue = CardClass.Value.three;
			RequiredValue.text = "3";
			break; 
		case CardClass.Value.three:
			W_Pile.RequiredValue = CardClass.Value.four;
			RequiredValue.text = "4";
			break; 
		case CardClass.Value.four:
			W_Pile.RequiredValue = CardClass.Value.five;
			RequiredValue.text = "5";
			break; 
		case CardClass.Value.five:
			W_Pile.RequiredValue = CardClass.Value.six;
			RequiredValue.text = "6";
			break; 

		case CardClass.Value.six:
			W_Pile.RequiredValue = CardClass.Value.seven;
			RequiredValue.text = "7";
			break; 
			
		case CardClass.Value.seven:
			W_Pile.RequiredValue = CardClass.Value.eight;
			RequiredValue.text = "8";
			break; 
		case CardClass.Value.eight:
			W_Pile.RequiredValue = CardClass.Value.nine;
			RequiredValue.text = "9";
			break; 
		case CardClass.Value.nine:
			W_Pile.RequiredValue = CardClass.Value.ten;
			RequiredValue.text = "10";
			break; 
		case CardClass.Value.ten:
			W_Pile.RequiredValue = CardClass.Value.jack;
			RequiredValue.text = "J";
			break; 
		case CardClass.Value.jack:
			W_Pile.RequiredValue = CardClass.Value.queen;
			RequiredValue.text = "Q";
			break; 
		case CardClass.Value.queen:
			W_Pile.RequiredValue = CardClass.Value.king;
			RequiredValue.text = "K";
			break; 
		case CardClass.Value.king:
			W_Pile.RequiredValue = CardClass.Value.one;
			RequiredValue.text = "A";
			break; 


		}

	}
	public void SwitchDown()
	{
		switch (W_Pile.RequiredValue) 

		{
		case CardClass.Value.one:
			W_Pile.RequiredValue = CardClass.Value.king;
			RequiredValue.text = "K";
			break; 
		case CardClass.Value.two:
			W_Pile.RequiredValue = CardClass.Value.one;
			RequiredValue.text = "A";
			break; 
		case CardClass.Value.three:
			W_Pile.RequiredValue = CardClass.Value.two;
			RequiredValue.text = "2";
			break; 
		case CardClass.Value.four:
			W_Pile.RequiredValue = CardClass.Value.three;
			RequiredValue.text = "3";
			break; 
		case CardClass.Value.five:
			W_Pile.RequiredValue = CardClass.Value.four;
			RequiredValue.text = "4";
			break; 

		case CardClass.Value.six:
			W_Pile.RequiredValue = CardClass.Value.five;
			RequiredValue.text = "5";
			break; 

		case CardClass.Value.seven:
			W_Pile.RequiredValue = CardClass.Value.six;
			RequiredValue.text = "6";
			break; 
		case CardClass.Value.eight:
			W_Pile.RequiredValue = CardClass.Value.seven;
			RequiredValue.text = "7";
			break; 
		case CardClass.Value.nine:
			W_Pile.RequiredValue = CardClass.Value.eight;
			RequiredValue.text = "8";
			break; 
		case CardClass.Value.ten:
			W_Pile.RequiredValue = CardClass.Value.nine;
			RequiredValue.text = "9";
			break; 
		case CardClass.Value.jack:
			W_Pile.RequiredValue = CardClass.Value.ten;
			RequiredValue.text = "10";
			break; 
		case CardClass.Value.queen:
			W_Pile.RequiredValue = CardClass.Value.jack;
			RequiredValue.text = "J";
			break; 
		case CardClass.Value.king:
			W_Pile.RequiredValue = CardClass.Value.queen;
			RequiredValue.text = "Q";

			break; 


		}

	}


	// Update is called once per frame
	public void SwitchTypesUp()
	{
		//W_Pile.switchonSprite ();
		switch (W_Pile.RequiredType)
		{
		case CardClass.Type.Spade:
			ChangeTypeButton.GetComponent <Image> ().sprite = Clubs;
			W_Pile.RequiredType = CardClass.Type.Club;
			W_Pile.switchonSprite ();
			break;
		case CardClass.Type.Club:
			W_Pile.RequiredType = CardClass.Type.Diamond;
			ChangeTypeButton.GetComponent <Image> ().sprite = Diamond;
			W_Pile.switchonSprite ();
			break;
		case CardClass.Type.Diamond:
			W_Pile.RequiredType = CardClass.Type.Heart;
			ChangeTypeButton.GetComponent <Image> ().sprite = Heart;
			W_Pile.switchonSprite ();
			break;
		case CardClass.Type.Heart:
			ChangeTypeButton.GetComponent <Image> ().sprite = Spades;
			W_Pile.RequiredType = CardClass.Type.Spade;
			W_Pile.switchonSprite ();
			break;



		}
	}
	public void SwitchTypesDown()
	{
		
		switch (W_Pile.RequiredType)
		{
		case CardClass.Type.Spade:
			ChangeTypeButton.GetComponent <Image> ().sprite = Heart;
			W_Pile.RequiredType = CardClass.Type.Heart;
			W_Pile.switchonSprite ();
			break;
		case CardClass.Type.Club:
			W_Pile.RequiredType = CardClass.Type.Spade;
			ChangeTypeButton.GetComponent <Image> ().sprite = Spades;
			W_Pile.switchonSprite ();
			break;
		case CardClass.Type.Diamond:
			W_Pile.RequiredType = CardClass.Type.Club;
			ChangeTypeButton.GetComponent <Image> ().sprite = Clubs;
			W_Pile.switchonSprite ();
			break;
		case CardClass.Type.Heart:
			ChangeTypeButton.GetComponent <Image> ().sprite = Diamond;
			W_Pile.RequiredType = CardClass.Type.Diamond;
			W_Pile.switchonSprite ();
			break;



		}
	}
}
