﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class SL_WinningCondition : MonoBehaviour {
	/*Developer Name: Dung Le

	Contribution: Winning Condition for the solitare mode



		Start & End dates: 11/9/2017 - 11/10/2017

		References:
		Links:*/
	public CardClass.Type RequiredType; 
	public CardClass.Value RequiredValue;
	public Sprite SpadeSprite;
	public Sprite ClubSprite;
	public Sprite DiamondSprite;
	public Sprite HeartSprite;
	private List<CardClass> AceList;
	private List<CardClass> TwoList;
	private List<CardClass> ThreeList;
	private List<CardClass> FourList;
	private List<CardClass> FiveList;
	private List<CardClass> SixList;
	private List<CardClass> SevenList;
	private List<CardClass> EightList;
	private List<CardClass> NineList;
	private List<CardClass> TenList;
	private List<CardClass> JList;
	private List<CardClass> QList;
	private List<CardClass> KList;

	private GameSystem GS;
	private SpriteRenderer _Sprite;
	// Use this for initialization
	void Start () {
		GS = GameObject.FindObjectOfType<GameSystem> ();
		_Sprite = GetComponentInChildren <SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void switchonSprite()
	{
		switch(RequiredType)
		{
		case CardClass.Type.Club:
			_Sprite.sprite = ClubSprite;
			break;
		case CardClass.Type.Spade:
			_Sprite.sprite = SpadeSprite;
			break;
		case CardClass.Type.Diamond:
			_Sprite.sprite = DiamondSprite;
			break;
		case CardClass.Type.Heart:
			_Sprite.sprite = HeartSprite;
			break;

		}

	}
	void OnTriggerStay(Collider other)
	{
		if (other.GetComponent <CardClass> () != null) {

			if (other.GetComponent <CardClass> ().CardType != RequiredType) {
				other.transform.position = other.GetComponent <CardClass> ().GetCurrentPos ();
			}
			if (other.GetComponent <CardClass> ().CardValue != RequiredValue) {
				other.transform.position = other.GetComponent <CardClass> ().GetCurrentPos ();
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent <CardClass> () != null) {
			
			if (other.GetComponent <CardClass> ().CardType == RequiredType) {

			

				if (other.GetComponent <CardClass> ().CardValue == RequiredValue) {


					switch (RequiredValue) {


					case CardClass.Value.one:
						/*/AceList.Add (other.GetComponent <CardClass> ());
						if (AceList.Count == 4) {
							RequiredValue = CardClass.Value.two;
						}*/
						break;
					case CardClass.Value.two:
						/*TwoList.Add (other.GetComponent <CardClass> ());
						if (TwoList.Count == 4) {
							RequiredValue = CardClass.Value.three;
						}*/
						break;
					case CardClass.Value.three:
						/*ThreeList.Add (other.GetComponent <CardClass> ());
						if (ThreeList.Count == 4) {
							RequiredValue = CardClass.Value.four;
						}*/
						break;
					case CardClass.Value.four:
					/*	FourList.Add (other.GetComponent <CardClass> ());
						if (FourList.Count == 4) {
							RequiredValue = CardClass.Value.five;
						}*/
						break;
					case CardClass.Value.five:
						/*FiveList.Add (other.GetComponent <CardClass> ());
						if (FiveList.Count == 4) {
							RequiredValue = CardClass.Value.six;
						}*/
						break;
					case CardClass.Value.six:
						/*SixList.Add (other.GetComponent <CardClass> ());
						if (SixList.Count == 4) {
							RequiredValue = CardClass.Value.seven;
						}*/
						break;
					case CardClass.Value.seven:
						/*SevenList.Add (other.GetComponent <CardClass> ());
						if (SevenList.Count == 4) {
							RequiredValue = CardClass.Value.eight;
						}*/
						break;
					case CardClass.Value.eight:
						/*EightList.Add (other.GetComponent <CardClass> ());
						if (EightList.Count == 4) {
							RequiredValue = CardClass.Value.nine;
						}*/
						break;

					case CardClass.Value.nine:
						/*NineList.Add (other.GetComponent <CardClass> ());
						if (NineList.Count == 4) {
							RequiredValue = CardClass.Value.ten;
						}*/
						break;
					case CardClass.Value.ten:
						/*TenList.Add (other.GetComponent <CardClass> ());

						if (TenList.Count == 4) {
							RequiredValue = CardClass.Value.jack;
						}*/
						break;
					case CardClass.Value.jack:
						/*JList.Add (other.GetComponent <CardClass> ());
						if (JList.Count == 4) {
							RequiredValue = CardClass.Value.queen;
						}*/
						break;

					case CardClass.Value.queen:
						/*QList.Add (other.GetComponent <CardClass> ());
						if (QList.Count == 4) {
							RequiredValue = CardClass.Value.king;
						}*/
						break;
					case CardClass.Value.king:
						/*KList.Add (other.GetComponent <CardClass> ());
						if (KList.Count == 4) {
							GS.CurrentGamestate = GameSystem.State.GameDone;
						}*/
						break;
					}
				}
			}

		}

	}
}
