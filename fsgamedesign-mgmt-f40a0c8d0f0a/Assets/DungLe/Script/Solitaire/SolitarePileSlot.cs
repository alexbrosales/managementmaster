﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolitarePileSlot : MonoBehaviour {
	/*Developer Name: Dung Le

	        Contribution: Original Slot that being distributed in the card in Solitare Mode

	               Feature

	               Start & End dates 11/8 - 11/10/2017

	               References:
	                      Links:*/
	public bool cardcontain;
	public DL_SolitareSLot Current_C;
	void OnTriggerEnter(Collider other)
	{
		if (Current_C == null) {
			if (other.GetComponent <DL_SolitareSLot> () != null) {
				cardcontain = true;
				Current_C = other.GetComponent <DL_SolitareSLot> ();
			}
		}
	}
	void OnTriggerExit(Collider other)
	{
		if(other.GetComponent <DL_SolitareSLot>()== Current_C)
		{
			cardcontain = false;
			Current_C = null;
		}
	}
}
