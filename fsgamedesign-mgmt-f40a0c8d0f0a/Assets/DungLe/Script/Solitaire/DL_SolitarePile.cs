﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SolitareMode{

public class DL_SolitarePile : MonoBehaviour {

		/*Developer Name: Dung Le

	        Contribution: Solitare Piles  in Solitare mode

	      

	              Start & End dates: 11/4/2017 - 11/10/2017

	               References:
	                      Links:*/
	public bool KingInOrNot;
	public GameObject NextPile;
		public List <SolitarePileSlot> OriginalPile;
	public List <GameObject> AllPiles;
	public GameObject CurrentFlippedCard;
	// Use this for initialization
	void Start () {
			
	}
	
	// Update is called once per frame
	void Update () {
			//CheckSlot ();	
	}

		public void CheckSlot()
		{
			if (OriginalPile [OriginalPile.Count - 1] != null) {
				if (OriginalPile [OriginalPile.Count - 1].cardcontain == true) {
					if (OriginalPile [OriginalPile.Count - 1].Current_C != null) {
						OriginalPile [OriginalPile.Count - 1].Current_C.gameObject.GetComponent <DL_SolitareSLot> ().Flipped = true;
					}
				}
				for (int i = 0; i<OriginalPile.Count-1; i++)
				{
					if (OriginalPile [i].cardcontain == true) {
						if (OriginalPile [i].Current_C != null) {
							OriginalPile [i].Current_C.gameObject.GetComponent <DL_SolitareSLot> ().Flipped = false;
						}
					}
				}
			}
		}
	public void SpawnCard()
	{
			CardClass[] AllCards = GameObject.FindObjectsOfType<CardClass> ();

			for (int i = 0; i< OriginalPile.Count; i++ ) {
				int x = Random.Range (0, 51);
				var cardSpawn =(GameObject) Instantiate (AllCards[x].gameObject,OriginalPile [i].transform.position, OriginalPile [i].transform.rotation);
				//AllCards [x].transform.position = OriginalPile [i].transform.position;


				print ("h1");
			}
		


	}
		void OnTriggerEnter(Collider other)
		{
			if(other.GetComponent <DL_SolitareSLot>() != null)
				
			{
				
				other.GetComponent <DL_SolitareSLot> ().FindNewSlot = true;
			}
		}

		void OnTriggerExit(Collider other)
		{
			if(other.GetComponent <DL_SolitareSLot>() != null)
			{
				other.GetComponent <DL_SolitareSLot> ().FindNewSlot = false;
			}
		}



}




}
