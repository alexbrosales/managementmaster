﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AR_Timer : MonoBehaviour {
	// Developer Name: Alex Rosales
	// Contribution: Timer Script
	// Feature: Camera animation
	// Start & End dates: 10/31/2017-11/1/2017
	// References: N/A
	// Links: N/A
	public bool TimerActive = true;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//print (TimerActive);
	}

	public void TimerSwitch()
	{
		TimerActive = !TimerActive;
	}
}
