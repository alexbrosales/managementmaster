﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AR_CameraSwitch : MonoBehaviour {

	// Developer Name: Alex Rosales
	// Contribution: Camera switch from 2d to 3d and visa versa
	// Feature: Camera animation
	// Start & End dates: 10/31/2017-11/1/2017
	// References: N/A
	// Links: N/A

	[SerializeField] Animator CameraAnim;
	// Use this for initialization
	void Start () {
		CameraAnim = this.GetComponent<Animator> ();
		CameraAnim.enabled = false;
	}
	// Update is called once per frame
	void Update () {
		if(CameraAnim.GetCurrentAnimatorStateInfo (0).IsName ("AR_Camera2DIdle") && CameraAnim.enabled == true){
			CameraAnim.enabled = false;
		}
		if(Input.GetKeyDown (KeyCode.Space))
		{
			if(CameraAnim.enabled == false)
			{
				CameraAnim.enabled = true;
				CameraAnim.Play ("AR_Camera2DTo3D");
				CameraAnim.SetBool ("Switch", false);
			} else {
				CameraAnim.SetBool ("Switch", true);
			}
		}
	}
}