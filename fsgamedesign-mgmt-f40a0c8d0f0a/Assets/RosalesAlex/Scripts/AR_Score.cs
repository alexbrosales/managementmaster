﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AR_Score : MonoBehaviour {
	// Developer Name: Alex Rosales
	// Contribution: Scoring UI Script
	// Feature: Camera zoom and pan
	// Start & End dates: 11/1/2017-Current
	// References: N/A
	// Links: N/A

	[SerializeField] private Text ScoreText;
	private int Score;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void IncreaseScore()
	{
		Score++;
		ScoreText.text = Score.ToString ("F0");
	}

	public void DecreaseScore()
	{
		if(Score > 0)
		{
			Score--;
			ScoreText.text = Score.ToString ("F0");
		}
	}
}
