﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AR_CameraZoom : MonoBehaviour {

	// Developer Name: Alex Rosales
	// Contribution: Camera can zoom in and out, Camera can pan around board
	// Feature: Camera zoom and pan
	// Start & End dates: 11/1/2017-Current
	// References: N/A
	// Links: N/A
	// Use this for initialization
	private bool MiddleMouseDown = false;
	private float Sensitivity = 0.2f;
	private float MouseX;
	private float MouseY;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//Set mouse position for panning
		MouseX = Input.GetAxis ("Mouse X") * Sensitivity;
		MouseY = Input.GetAxis ("Mouse Y") * Sensitivity;

		//If the mousewheel is scrolling up....
		if(Input.GetAxis ("Mouse ScrollWheel") > 0 )
		{
			//Keep camera from zooming too far in
			if (this.GetComponent<Camera> ().fieldOfView >= 25) {
				//Zoom in
				this.GetComponent<Camera> ().fieldOfView -= 2.0f;
			}
		} else {
		//If the mousewheel is scrolling down...
			if(Input.GetAxis ("Mouse ScrollWheel") < 0)
			{
				//Keep the camera from zooming too far out
				if (this.GetComponent<Camera> ().fieldOfView <= 75) {
					//Zoom out
					this.GetComponent<Camera> ().fieldOfView += 2.0f;
				}
			}
		}
		//Check if middle mouse button is being held down
		if(Input.GetMouseButtonDown (2))
		{
			MiddleMouseDown = true;
		}
		//Check if middle mouse button is let up
		if(Input.GetMouseButtonUp (2))
		{
			MiddleMouseDown = false;
		}
		//If middle mouse button is being held down...
		if (MiddleMouseDown == true){
			float ZPos = this.transform.position.z;
			float XPos = this.transform.position.x;
			float YPOS = this.transform.position.y;
			//Constrain panning
			if (ZPos > -10) {
				if(ZPos < 10){
					if (XPos > -10) {
						if (XPos < 10) {
							//Pan camera
							transform.Translate (MouseX, MouseY, 0);
						} else {
							transform.position = new Vector3 (9.9f, YPOS, ZPos);
						}
					} else {
						transform.position = new Vector3 (-9.9f, YPOS, ZPos);
					}
				} else {
					transform.position = new Vector3(XPos, YPOS, 9.9f);
				}
			} else {
				transform.position =  new Vector3(XPos, YPOS, -9.9f);
			}
		}
	}
}