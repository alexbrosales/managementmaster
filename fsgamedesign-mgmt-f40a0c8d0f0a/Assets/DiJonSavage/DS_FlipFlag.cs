﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//

public class DS_FlipFlag : MonoBehaviour {
	/*Developer Name: Dijon Savage

	        Contribution: Card Movement

	               Feature

	               Start & End dates 11/1 - 11/10/2017

	               References:
	                      Links:*/

	bool start = false;
	[SerializeField] int mouseClicks = 0;
	[SerializeField] float mouseTimer = 0.0f;

	public GameObject face;
	public GameObject back;

	[SerializeField] bool can180 = true;
	bool overCard = false;

	float flipTime = 0.1f;

	Color lightRed = new Color (1f, 0.69f, 0.69f);
	Color highRed = new Color (1f, 0.25f, 0.25f);

	public Material baseColor;
	public Material baseBackColor;

	Vector3 pPoint;
	Vector3 set;

	// Use this for initialization
	void Start () 
	{
		//Do not transform the parent GameObject
		transform.parent = null;

	}
	
	// Update is called once per frame
	void Update () 
	{
		//Start timer and count by deltaTime
		mouseTimer += Time.deltaTime;

		if(can180)
		{
			//Can flip cards

		}
		if(!can180)
		{
			//Cannot flip the card

		}

		RotateCards ();
	}

	public void ResetTime()
	{
		//Reset the time that passed
		mouseTimer = 0.0f;
	}

	void OnMouseUp()
	{
		if(mouseTimer > flipTime)
		{
			//The player is dragging the card
		}
		else if(can180)
		{
			// if the player is not dragging the card, flip it if able
			transform.Rotate (0, 0, 180);
		}
		else
		{
			//do not flip the card
			transform.Rotate (0, 0, 0);
		}
	}

	void OnMouseOver()
	{
		overCard = true;
		gameObject.GetComponent<Light> ().enabled = true;
	}

	void OnMouseExit()
	{
		overCard = false;
		gameObject.GetComponent<Light> ().enabled = false;
	}

	void RotateCards()
	{
		if(overCard && Input.GetKeyDown (KeyCode.A))
		{
			transform.Rotate (-90, 0, 0);
		}

		if(overCard && Input.GetKeyDown (KeyCode.D))
		{
			transform.Rotate (90, 0, 0);
		}
	}

	//Flipping the Card
	void OnMouseDown()
	{
		ResetTime ();
		//when the player clicks, the counter goes up by one
		mouseClicks++;
		if(start)
		{
			//only count if the mouse has been left clicked
			return;
		}
		start = true;

		//check the number of times the mouse has been clicked within the set time
		Invoke ("CheckMouseClicks", mouseTimer);

		pPoint = Camera.main.WorldToScreenPoint (transform.position);
		set = transform.position - Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, pPoint.z));
	}

	void CheckMouseClicks()
	{
		if(mouseClicks >= 2 && can180 == true)
		{
			//activate set marker
			face.GetComponent<Renderer> ().material.color = lightRed;
			back.GetComponent<Renderer> ().material.color = highRed;
			can180 = false;
		}
		else if(mouseClicks >= 2 && can180 == false)
		{
			face.GetComponent<Renderer> ().material = baseColor;
			back.GetComponent<Renderer> ().material = baseBackColor;
			can180 = true;
		}
		start = false;
		mouseClicks = 0;
	}

	//Dragging the Cards
	public void OnMouseDrag()
	{
		Vector3 curpPoint = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, pPoint.z);
		Vector3 curPosition = Camera.main.ScreenToWorldPoint (curpPoint) + set;
		transform.position = curPosition + new Vector3 (0, 3f, 0);
	}

}
