﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Christopher Davis
// Contribution: Making variables to accessed by the pile parent scripts
// Feature: visual buttons for Pile Interactions
// Date: 11/15/2017

public class ButtonSystem : MonoBehaviour {

    [SerializeField]
    Button cutDeckButton;
    bool cutDeck;

    public void ToggleCutDeck()
    {
        cutDeck = !cutDeck;
    }

    public bool GetCutDeck()
    {
        return cutDeck;
    }


    bool makePile;

    public void ToggleMakePile()
    {
        makePile = !makePile;
    }

    public bool GetMakePile()
    {
        return makePile;
    }


    bool fanCards;

    public void ToggleFanCards()
    {
        fanCards = !fanCards;
    }

    public bool GetFanCards()
    {
        return fanCards;
    }


    bool shuffleDeck;

    public void ToggleShuffleDeck()
    {
        shuffleDeck = !shuffleDeck;
    }

    public bool GetShuffleDeck()
    {
        return shuffleDeck;
    }

}
