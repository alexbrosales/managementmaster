﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piles : MonoBehaviour {

    // Developer Name: Christopher Davis
    // Contribution: Creating Piles of cards, also some variables PileParent will access 
    // Feature: Card Piles
    // Start & End dates: 11/1/2017 - 11/10/2017
    // References: N/A
    // Links: N/A


    [SerializeField] private GameObject PileFab;

    bool pileMade = false;

    float distBtwnCards = .025f;

    public float GetDistBtwnCards()
    {
        return distBtwnCards;
    }


    [SerializeField] int cardDrawOrientation = 0;

    public int GetcardDrawOrientation()
    {
        return cardDrawOrientation;
    }

    public void SetCardFlipOrientation(int flipOrien)
    {
        cardDrawOrientation = flipOrien;
    }


    [SerializeField] int cardPickUpOrien = 0;

    public int GetCardPickUpOrien()
    {
        return cardPickUpOrien;
    }

    public void SetCardPickUpOrien(int Oreientation)
    {// totatlly correct spelling
        cardPickUpOrien = Oreientation;
    }


    [SerializeField] int cardDrawSide = 0;

    public int GetCardDrawSide()
    {
        return cardDrawSide;
    }

    public void SetCardDrawSide(int drawSide)
    {
        cardDrawSide = drawSide;
    }


    [SerializeField] int shuffleStyle = 0;

    public int GetShuffleStyle()
    {
        return shuffleStyle;
    }

    public void SetShuffleStyle(int style)
    {
        shuffleStyle = style;
    }


    [SerializeField] int FannedOrien;

    public int GetFannedOrien()
    {
        return FannedOrien;
    }

    public void SetFannedOrien(int Orien)
    {
        FannedOrien = Orien;
    }
    

    float timer = 0;

    ButtonSystem buttons;

    void Start() {
        buttons = GameObject.FindObjectOfType<ButtonSystem>();
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.LeftShift) || (buttons.GetMakePile() && (Input.GetMouseButton(0) || Input.GetMouseButtonUp(0))))
        {
            Ray MouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit MouseRayHit;

            if (!Input.GetMouseButton(0)/* || (buttons.GetMakePile() && Input.GetMouseButtonUp(0))*/)
            {

                //print("space Pressed");

                //List<GameObject> CardsFound = new List<GameObject>();

            CreatePile(MouseRay);

            }//end if mouse button is not being pressed
            else
            {
                if (!pileMade)
                {
                    List<GameObject> CardsFound = new List<GameObject>();
                    

                    //if a card is under the mouse
                    if (Physics.Raycast(MouseRay, out MouseRayHit) && MouseRayHit.collider.tag == "Card")
                    {
                        if (MouseRayHit.collider.GetComponent<PileParent>())
                        {
                            // do nothing if it is a pile, that is taken care of in PileParent script
                        }
                        else
                        {
                            GameObject PileParent = Instantiate(PileFab, MouseRayHit.collider.transform.position + new Vector3(0, -.2f, 0), Quaternion.Euler(0, 90, 90), MouseRayHit.transform);

                            pileMade = true;

                            CardsFound.Add(MouseRayHit.collider.gameObject);
                            SetCardOrien(CardsFound[0]);

                            PileParent.GetComponent<PileParent>().SetCards(CardsFound);

                            //PileParent.transform.parent = MouseRayHit.transform;

                            PileParent.GetComponent<PileParent>().HeldAtStart(MouseRayHit.collider.gameObject);

                        }
                    }
                }
            }
        }//end if button pressed
        else
        {

        }

        if (Input.GetMouseButtonUp(0))
        {
            if (pileMade)
            {
                pileMade = false;
            }
        }
    }

    RaycastHit CheckIfPile(Ray MouseRay, RaycastHit MouseRayHit)
    {
        if (MouseRayHit.collider.GetComponent<PileParent>())
        {
            MouseRayHit.collider.GetComponent<PileParent>().DisbandPile();
            Physics.Raycast(MouseRay, out MouseRayHit);

            return MouseRayHit;
        }
        else
            return MouseRayHit;
    }

    public void SetCardOrien(GameObject card)
    {
        if (cardPickUpOrien == 0)
        {
            if (Vector3.Dot(Vector3.up, card.transform.right * -1) >= 0)
            {
                card.transform.rotation = Quaternion.Euler(0, 90, -90);
            }
            else
            {
                card.transform.rotation = Quaternion.Euler(0, 90, 90);
            }
        }

        if (cardPickUpOrien == 1)
        {
            card.transform.rotation = Quaternion.Euler(0, 90, 90);
        }
        else if(cardPickUpOrien == 2)
        {
            card.transform.rotation = Quaternion.Euler(0, 90, -90);
        }
    }



    public GameObject CreatePile(Ray Ray)
    {
        RaycastHit MouseRayHit;

        List<GameObject> CardsFound = new List<GameObject>();

        //if a card is under the mouse
        if (Physics.Raycast(Ray, out MouseRayHit) && MouseRayHit.collider.tag == "Card")
        {
            MouseRayHit = CheckIfPile(Ray, MouseRayHit);

            CardsFound.Add(MouseRayHit.collider.gameObject);

            bool cardFound = true;
            int LastCard = 0;

            print("foundCard");

            //look for more cards
            while (cardFound)
            {
                Ray newRay = new Ray(MouseRayHit.point + new Vector3(0, .1f, 0), Vector3.down);

                //move the last card so I don't hit it again
                CardsFound[LastCard].transform.position += new Vector3(0, -100, 0);

                if (Physics.Raycast(newRay, out MouseRayHit) && MouseRayHit.collider.tag == "Card")
                {
                    MouseRayHit = CheckIfPile(newRay, MouseRayHit);

                    cardFound = true;
                    CardsFound.Add(MouseRayHit.collider.gameObject);

                    LastCard++;
                }
                else
                {
                    cardFound = false;
                }
            }//end of while

            if (CardsFound.Count > 1)
            {
                Vector3 averagePos = new Vector3();

                for (int i = 0; i < CardsFound.Count; i++)
                {
                    CardsFound[i].transform.position += new Vector3(0, 100, 0);
                    averagePos += CardsFound[i].transform.position;

                    CardsFound[i].GetComponent<Rigidbody>().isKinematic = true;
                    CardsFound[i].GetComponent<Collider>().enabled = false;

                    SetCardOrien(CardsFound[i]);
                }

                averagePos /= CardsFound.Count;

                for (int i = CardsFound.Count - 1; i >= 0; i--)
                {
                    CardsFound[i].transform.position = new Vector3(averagePos.x, CardsFound[CardsFound.Count - 1].transform.position.y + (((CardsFound.Count - 1) - i) * distBtwnCards), averagePos.z);
                }


                // this is the parent object being made
                GameObject PileParent;

                if (CardsFound.Count % 2 == 0)
                {
                    PileParent = Instantiate(PileFab, new Vector3(averagePos.x, CardsFound[CardsFound.Count / 2].transform.position.y, averagePos.z), Quaternion.Euler(0, 90, 90));
                }
                else
                {
                    //This is a point vertically between the two middle cards in the array
                    float halfwayHeight = (CardsFound[Mathf.FloorToInt(CardsFound.Count / 2)].transform.position.y + CardsFound[Mathf.CeilToInt(CardsFound.Count / 2)].transform.position.y) / 2;

                    PileParent = Instantiate(PileFab, new Vector3(averagePos.x, halfwayHeight, averagePos.z), Quaternion.Euler(0, 90, 90));
                }
                //end parent made


                PileParent.GetComponent<BoxCollider>().size = new Vector3(.1f + (distBtwnCards * CardsFound.Count), PileParent.GetComponent<BoxCollider>().size.y, PileParent.GetComponent<BoxCollider>().size.z);


                for (int i = 0; i < CardsFound.Count; i++)
                {
                    CardsFound[i].transform.parent = PileParent.transform;
                    //Destroy(CardsFound[i]);
                }
                PileParent.GetComponent<PileParent>().SetCards(CardsFound);

                return PileParent;
            }
            else
            {
                CardsFound[0].transform.position += new Vector3(0, 100, 0);

                return null;
            }

        }
        return null;
    }

    public float GetTime()
    {
        return timer;
    }

    public void ResetTimer()
    {
        timer = 0;
    }
}
