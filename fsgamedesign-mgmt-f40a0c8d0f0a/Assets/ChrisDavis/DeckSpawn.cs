﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckSpawn : MonoBehaviour {

    // Developer Name: Christopher Davis
    // Contribution: Spawn a deck of cards 
    // Feature: Spawn in a deck
    // Start & End dates: 11/8/2017 - 11/9/2017
    // References: N/A
    // Links: N/A

    [SerializeField] GameObject[] cards;
    //List<GameObject> cardsInPlay = new List<GameObject>();

    [SerializeField] Transform spawnPoint;

    [SerializeField] float spawnCardDistance = .025f;
    
    Piles pileScript;
    /*
    [SerializeField] GameObject pileFab;
    GameObject pileInPlay;
    */

	// Use this for initialization
	void Start () {

        for (int i = 0; i < cards.Length; i++)
        {
            Instantiate(cards[i], spawnPoint.position + (new Vector3(0, ((cards.Length / 2) - i) * spawnCardDistance, 0)), spawnPoint.rotation);
        }

        pileScript = GameObject.FindObjectOfType<Piles>();

	}

    public void SpawnDeck()
    {
        for (int i = 0; i < cards.Length; i++)
        {
            Instantiate(cards[i], spawnPoint.position + (new Vector3(0, ((cards.Length / 2) - i) * spawnCardDistance, 0)), spawnPoint.rotation);
        }

        pileScript.CreatePile(new Ray(spawnPoint.position + new Vector3(0, 12, 0), Vector3.down));
    }


	
}
