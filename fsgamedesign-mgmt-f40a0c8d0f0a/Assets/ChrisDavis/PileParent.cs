﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Developer Name: Christopher Davis
// Contribution: Manipulating, adding, and removing cards in a pile
// Feature: Card Piles, riffle shuffle
// Start & End dates: 11/3/2017 - 11/  /2017
// References: Answer by username Fede from a question on Stackoverflow for how to move something to the front of a list
// Links: https://stackoverflow.com/questions/2575592/moving-a-member-of-a-list-to-the-front-of-the-list

public class PileParent : MonoBehaviour {

   [SerializeField] List<GameObject> Cards;

    bool Fanned = false;

    bool removeCardButtonPressed = false;

    // These variables are currently used to do things in the Riffle Shuffle state and initial function
    float timer = 0;

    GameObject[] Cards1;
    GameObject[] Cards2;

    Vector3[] prevPos;

    int halfOfCards;

    int card1Index = 0;
    int card2Index = 0;

    int CardsIndex = 0;
    // This is the end of the above mentioned list

    float distBtwnCards;

    int curFannedOrien;

    Piles pileScript;

    ButtonSystem buttons;

    bool OnlyChangeCollidersOnce = false;

    enum States
    {
        HELD, 
        NOT_HELD,
        RIFFLE_SHUFFLE,
        FANNED
    }

    States CurState = States.NOT_HELD;

    Dictionary<States, Action> StateMachine = new Dictionary<States, Action>();

    GameObject tempParent;

    // Use this for initialization
    void Awake() {
        StateMachine.Add(States.HELD, HeldState);
        StateMachine.Add(States.NOT_HELD, NotHeldState);
        StateMachine.Add(States.RIFFLE_SHUFFLE, RiffleShuffleState);
        StateMachine.Add(States.FANNED, FannedState);

        pileScript = GameObject.FindObjectOfType<Piles>();
        buttons = GameObject.FindObjectOfType<ButtonSystem>();

        distBtwnCards = pileScript.GetDistBtwnCards();

	}

    // Update is called once per frame
    void Update() {

        StateMachine[CurState].Invoke();
        print("");

    }


    //------------------------------------------------------------------------------------
    void NotHeldState()
    {
        //setup for removing a card the long way
        if (Input.GetMouseButtonDown(1) )
        {
            gameObject.GetComponent<Collider>().enabled = false;
            gameObject.GetComponent<Rigidbody>().isKinematic = true;
            IsCardColliding(true);

            removeCardButtonPressed = true;

            print("Slow Way");
        }

        //if you want to add another button that keeps the pile from changing states
        // add the button check in this if-else chain before the final else statement
        if (removeCardButtonPressed)
        {
            print("I am true");
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit mouseRayHit = GetRayCastFromMouse();
                //print("I am Here");

                if (mouseRayHit.collider.transform.parent == gameObject.transform)
                {
                    RemoveCard(mouseRayHit.collider.gameObject, true, true);
                    
                    //print("Should be done");
                }
            }
        }
        else if ((Input.GetKey(KeyCode.Q) && Input.GetMouseButtonDown(0)) || (buttons.GetCutDeck() && Input.GetMouseButtonDown(0)))
        {

            if (GetRayCastFromMouse().collider.gameObject == gameObject)
            {
                cutPile(Cards.Count / 2, true);
            }

            ChangeStates(States.HELD);
        }
        else
        {//    change to held state here
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit mouseRayHit = GetRayCastFromMouse();

                if (mouseRayHit.collider.gameObject == gameObject)
                {
                    ChangeStates(States.HELD);
                }
            }
        }

        //unsetup removing a card the long way
        if (Input.GetMouseButtonUp(1))
        {
            gameObject.GetComponent<Collider>().enabled = true;
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
            IsCardColliding(false);

            removeCardButtonPressed = false;
        }
        
        // shuffle
        if (Input.GetKeyDown(KeyCode.E) || (Input.GetMouseButtonDown(0) && buttons.GetShuffleDeck()))
        {
            Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit mouseRayHit;

            //print("I am Here");

            if (Physics.Raycast(mouseRay, out mouseRayHit) && mouseRayHit.collider.gameObject == gameObject)
            {
                if (pileScript.GetShuffleStyle() == 0)
                {
                    RiffleShuffle();
                }
                else if (pileScript.GetShuffleStyle() == 1)
                {
                    RandomShuffle();
                }
            }
        }

        //make all cards face up
        if (Input.GetKeyDown(KeyCode.W))
        {
            RaycastHit mouseRayHit = GetRayCastFromMouse();

            print("trying to make things face up");

            if (mouseRayHit.collider.gameObject == gameObject)
            {
                foreach (GameObject card in Cards)
                {
                    if (IsCardFacingUp(card))
                    {

                    }
                    else
                    {
                        card.transform.Rotate(0, 180, 0);
                    }
                }
                print("end of foreach loop");
            }
        }

        //make all cards face down
        if (Input.GetKeyDown(KeyCode.S))
        {
            RaycastHit mouseRayHit = GetRayCastFromMouse();

            if (mouseRayHit.collider.gameObject == gameObject)
            {
                foreach (GameObject card in Cards)
                {
                    if (IsCardFacingUp(card))
                    {
                        card.transform.Rotate(0, 180, 0);
                    }
                    else
                    {

                    }
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.F) || (buttons.GetFanCards() && Input.GetMouseButtonDown(0)))
        {
            if (GetRayCastFromMouse().collider.gameObject == gameObject)
            {
                FanCards();
            }
        }

        //not used Right Now
        if (Input.GetMouseButtonDown(0))
        {

        }
    }
    // ---------------------------------------------------------------------




    // ---------------------------------------------------------------------
    void HeldState()
    {
        if (Input.GetMouseButtonDown(1))
        {
            pileScript.ResetTimer();

            DrawCard();
        }

        if (Input.GetMouseButton(1))
        {
            if (pileScript.GetTime() > .2f)
            {
                pileScript.ResetTimer();

                DrawCard();
            }
        }

        if (Input.GetKey(KeyCode.LeftShift) || buttons.GetMakePile())
        {
            Ray rayDown = new Ray(gameObject.transform.position, Vector3.down);
            RaycastHit rayDownHit;

            if (Physics.Raycast(rayDown, out rayDownHit) && rayDownHit.collider.tag == "Card"){
                if (rayDownHit.collider.GetComponent<PileParent>())
                {
                    /* This is if I want to drag over a pile to collect that pile as well
                    rayDownHit.collider.GetComponent<PileParent>().DisbandPile();

                    Physics.Raycast(rayDown, out rayDownHit);
                    */
                }
                else
                {
                    AddCard(rayDownHit.collider.gameObject);
                }
            }
        }

        //get rid of this pile object if it is out of cards
        if (Input.GetMouseButtonUp(0))
        {
            if (Cards.Count == 1)
            {
                RemoveCard(Cards[0], true, true);
            }
            else if (Cards.Count == 0)
            {
                Destroy(gameObject);
            }
            else
            {// change states to HELD
                ChangeStates(States.NOT_HELD);

                // This is in case the pile started in this state, and is being dragged
                // around by a card the pile owns instead of the other way around
                if (tempParent != null)
                {
                    gameObject.transform.parent = null;
                    gameObject.GetComponent<Rigidbody>().isKinematic = false;

                    tempParent.transform.parent = gameObject.transform;

                    if (IsCardFacingUp(tempParent))
                    {
                        tempParent.transform.rotation = Quaternion.Euler(0, 90, 90);
                    }
                    else
                    {
                        tempParent.transform.rotation = Quaternion.Euler(0, 90, -90);
                    }
                    tempParent.GetComponent<Collider>().enabled = false;
                    tempParent.GetComponent<Rigidbody>().isKinematic = true;

                    tempParent = null;
                }

                ResetCardPositions();
            }
        }

        if (tempParent != null)
        {
            transform.position = tempParent.transform.position + new Vector3(0, -.2f, 0);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            foreach (GameObject card in Cards)
            {
                if (/*Vector3.Dot(card.transform.right, Vector3.up) >= 0*/ IsCardFacingUp(card))
                {

                }
                else
                {
                    card.transform.Rotate(0, 180, 0);
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            foreach (GameObject card in Cards)
            {
                if (IsCardFacingUp(card))
                {
                    card.transform.Rotate(0, 180, 0);
                }
                else
                {

                }
            }
        }
    }
    // -------------------------------------------------------------




    // --------------------------------------------------------------
    void RiffleShuffleState()
    {
        timer += Time.deltaTime * 4;


        if (timer >= 0 && timer < 1)
        {
            for (int i = 0; i < Cards1.Length; i++)
            {
                Cards1[i].transform.localPosition = Vector3.Lerp(prevPos[i], new Vector3(0, 0, -2), timer);
            }
            for (int i = 0; i < Cards2.Length; i++)
            {
                Cards2[i].transform.localPosition = Vector3.Lerp(prevPos[i + Cards1.Length], new Vector3(0, 0, 2), timer);
            }
        }
        else if (timer >= 1 && timer < 2)
        {
            for (int i = 0; i < Cards1.Length; i++)
            {
                Cards1[i].transform.localPosition = new Vector3(0, 0, -2);
                prevPos[i] = Cards1[i].transform.localPosition;
            }
            for (int i = 0; i < Cards2.Length; i++)
            {
                Cards2[i].transform.localPosition = new Vector3(0, 0, 2);
                prevPos[i + Cards1.Length] = Cards2[i].transform.localPosition;
            }

            timer = 2;
        }
        else if (timer >= 2 && timer < 3)
        {
            for (int i = 0; i < Cards1.Length; i++)
            {
                Cards1[i].transform.localPosition = Vector3.Lerp(prevPos[i], new Vector3((halfOfCards - Cards.IndexOf(Cards1[i])) * distBtwnCards, 0, 0), timer - 2);
            }
            for (int i = 0; i < Cards2.Length; i++)
            {
                Cards2[i].transform.localPosition = Vector3.Lerp(prevPos[i + Cards1.Length], new Vector3((halfOfCards - Cards.IndexOf(Cards2[i])) * distBtwnCards, 0, 0), timer - 2);
            }

            /*
            for (int i = 0; i < Cards.Count; i++)
            {
                Cards[i].transform.localPosition = Vector3.Lerp(prevPos[i], new Vector3((halfOfCards - i) * .1f, 0, 0), timer - 2);
            }
            */
        }
        else if (timer >= 3)
        {
            ResetCardPositions();

            if (Input.GetMouseButton(0))
            {

                RaycastHit MouseRayHit = GetRayCastFromMouse();

                if (MouseRayHit.collider.gameObject == gameObject)
                {
                    ChangeStates(States.HELD);
                }
                else
                {
                    ChangeStates(States.NOT_HELD);
                }
            }
            else
            {
                ChangeStates(States.NOT_HELD);
            }
        }
    }
    // ---------------------------------------------------------------




    // ----------------------------------------------------------------
    void FannedState()
    {
        if (Input.GetKeyDown(KeyCode.F) || (Input.GetMouseButtonDown(0) && buttons.GetFanCards()))
        {
            if (GetRayCastFromMouse().collider.gameObject == gameObject)
            {
                FanCards();
            }
        }

        if (Input.GetMouseButtonDown(1) || (buttons.GetCutDeck() && !OnlyChangeCollidersOnce))
        {
            OnlyChangeCollidersOnce = true;

            gameObject.GetComponent<Collider>().enabled = false;
            gameObject.GetComponent<Rigidbody>().isKinematic = true;
            IsCardColliding(true);

            removeCardButtonPressed = true;

            print("Slow Way");
        }

        if (removeCardButtonPressed)
        {
            print("I am true");

            //to remove a pile from the fanned state
            if (Input.GetKeyDown(KeyCode.Q) || (OnlyChangeCollidersOnce && Input.GetMouseButtonDown(0)))
            {
                RaycastHit mouseRayHit = GetRayCastFromMouse();

                buttons.ToggleCutDeck();

                if (Cards.Contains(mouseRayHit.collider.gameObject))
                {
                    for (int i = 0; i <= Cards.IndexOf(mouseRayHit.collider.gameObject);)
                    {
                        Cards[i].transform.position = mouseRayHit.point + new Vector3(0,3,0);

                        RemoveCard(Cards[i], true, true);
                    }

                    pileScript.CreatePile(new Ray(mouseRayHit.point + new Vector3(0,10,0), Vector3.down)).GetComponent<PileParent>().FanCards();
                }
            }
            else if (Input.GetMouseButtonDown(0) && !buttons.GetCutDeck())
            {
                RaycastHit mouseRayHit = GetRayCastFromMouse();
                //print("I am Here");

                if (mouseRayHit.collider.transform.parent == gameObject.transform)
                {
                    RemoveCard(mouseRayHit.collider.gameObject, true, true);

                    //print("Should be done");
                }
            }
        }

        if (Input.GetMouseButtonUp(1) || (!buttons.GetCutDeck() && OnlyChangeCollidersOnce))
        {
            OnlyChangeCollidersOnce = false;

            gameObject.GetComponent<Collider>().enabled = true;
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
            IsCardColliding(false);

            removeCardButtonPressed = false;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (GetRayCastFromMouse().collider.gameObject == gameObject)
            {
                ResetCardPositions();
            }
        }
    }
    //-------------------------------------------------------------------



    void ChangeStates(States state)
    {
        CurState = state;
    }

    //used by another script when making a pile that is being held by the player when being made
    public void HeldAtStart(GameObject curParent)
    {
        CurState = States.HELD;

        tempParent = curParent;

        if (transform.parent == null)
        {
            transform.parent = curParent.transform;
        }

        GetComponent<BoxCollider>().size = new Vector3(.1f, 3.7f,2.7f);
        GetComponent<Rigidbody>().isKinematic = true;
    }

    public void SetCards(List<GameObject> Pile)
    {
        Cards = Pile;
    }

    public void RemoveCard(GameObject missingCard, bool willDestroy, bool resetPos)
    {
        Cards.Remove(missingCard);

        missingCard.GetComponent<Rigidbody>().isKinematic = false;
        missingCard.GetComponent<Rigidbody>().velocity = new Vector3();
        missingCard.GetComponent<Collider>().enabled = true;
        missingCard.transform.parent = null;

        missingCard.tag = "Card";

        if (pileScript.GetcardDrawOrientation() == 1)
        {
            missingCard.transform.rotation = Quaternion.Euler(0, 90, 90);
        }

        if (pileScript.GetcardDrawOrientation() == 2)
        {
            missingCard.transform.rotation = Quaternion.Euler(0, 90, -90);
        }
        
        if (willDestroy)
        {
            if (Cards.Count == 1)
            {
                RemoveCard(Cards[0], willDestroy, true);
                Destroy(gameObject);
            }
            else if (Cards.Count == 0)
            {
                Destroy(gameObject);
            }
            else if (missingCard == null)
            {
                Destroy(gameObject);
            }
        }
        if (resetPos)
        {
            ResetCardPositions();
        }
    }

    public void AddCard(GameObject newCard)
    {
        Cards.Add(newCard);

        newCard.GetComponent<Rigidbody>().isKinematic = true;
        newCard.GetComponent<Collider>().enabled = false;

        if(newCard.transform.childCount <= 2)
            newCard.transform.parent = gameObject.transform;

        if (pileScript.GetCardPickUpOrien() == 1)
        {
            newCard.transform.rotation = Quaternion.Euler(0, 90, 90);
        }

        if (pileScript.GetCardPickUpOrien() == 2)
        {
            newCard.transform.rotation = Quaternion.Euler(0, 90, -90);
        }

        ResetCardPositions();
    }

    public void cutPile(int indexCutOff, bool direction)
    {
        for (int i = Cards.Count - 1; i >= indexCutOff; i--)
        {
            RemoveCard(Cards[i], false, true);
        }

        //gameObject.transform.position += new Vector3(0, 3, 0);

        pileScript.CreatePile(new Ray(gameObject.transform.position + new Vector3(0, 0, 0), Vector3.down));
    }

    public void MoveToTop(GameObject movingCard)
    {
        int index = Cards.IndexOf(movingCard);

        for (int i = index; i > 0; i--)
            Cards[i] = Cards[i - 1];
        Cards[0] = movingCard;

        ResetCardPositions();
    }

    //unnessecary for now
    private void MoveCollider(int posOrNeg1)
    {
        foreach (GameObject card in Cards)
        {
            card.transform.parent = null;
        }

        gameObject.transform.position += new Vector3(0,100 * posOrNeg1,0);
        
        foreach (GameObject card in Cards)
        {
            card.transform.parent = gameObject.transform;
        }
        
    }

    private void IsCardColliding(bool Colliding)
    {
        foreach (GameObject card in Cards)
        {
            card.GetComponent<Collider>().enabled = Colliding;
        }
    }

    public void RiffleShuffle()
    {
        print("shuffling");

        timer = 0;

        halfOfCards = Mathf.CeilToInt(Cards.Count/2);

        Cards1 = new GameObject[halfOfCards];
        Cards2 = new GameObject[Cards.Count - halfOfCards];

        card1Index = 0;
        card2Index = 0;

        CardsIndex = 0;

        prevPos = new Vector3[Cards.Count];

        for (int i = halfOfCards-1; i >= 0; i--)
        {
            Cards1[i] = Cards[i];
            //Cards[i].transform.position += Vector3.right * distanceMoved;

            prevPos[i] = Cards1[i].transform.localPosition;
        }

        for (int i = halfOfCards; i < Cards.Count; i++)
        {
            Cards2[i - halfOfCards] = Cards[i];
            //Cards[i].transform.position -= Vector3.right * distanceMoved;

            prevPos[i] = Cards2[i - halfOfCards].transform.localPosition;
        }
        
        for (int f = 0; f < Cards.Count;)
        {
            Cards.Remove(Cards[f]);
        }

        //overwrite old card placement
        while (card2Index < Cards2.Length || card1Index < Cards1.Length)
        {
            int rand = UnityEngine.Random.Range(1, 5);
            
            //add cards to the main deck from the second of the two
            if (card2Index < Cards2.Length)
            {
                for (int i = 0; i < rand && card2Index < Cards2.Length; i++)
                {
                    if (card2Index < Cards2.Length)
                    {
                        Cards.Add(Cards2[card2Index]);
                        //Cards2[card2Index] = null;
                        //Cards[CardsIndex].transform.localPosition = new Vector3((halfOfCards - CardsIndex) * .1f,0, 0);

                        card2Index++;
                        CardsIndex++;
                    }
                }
            }

            rand = UnityEngine.Random.Range(1, 5);

            //add cards to the main deck from the first of the two
            if (card1Index < Cards1.Length)
            {
                for (int i = 0; i < rand && card1Index < Cards1.Length; i++)
                {
                    
                    if (card1Index < Cards1.Length)
                    {
                        Cards.Add(Cards1[card1Index]);
                        //Cards1[card1Index] = null;
                        //Cards[CardsIndex].transform.localPosition = new Vector3((halfOfCards - CardsIndex) * .1f,0, 0);

                        card1Index++;
                        CardsIndex++;
                    }
                }
            }
           
        }// end of while

        ChangeStates(States.RIFFLE_SHUFFLE);
    }

    public void RandomShuffle()
    {
        List<GameObject> CardsNew = new List<GameObject>();

        for (int i = 0; i < Cards.Count;)
        {
            int rand = UnityEngine.Random.Range(0, Cards.Count);
            CardsNew.Add(Cards[rand]);

            Cards.Remove(Cards[rand]);
        }

        Cards = CardsNew;

        ResetCardPositions();
    }

    public void DisbandPile()
    {
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        gameObject.GetComponent<Collider>().enabled = false;

        for (int i = 0; i < Cards.Count;)
        {
            RemoveCard(Cards[i], true, true);
        }

        Destroy(gameObject);
    }

    public void DropCard(GameObject card)
    {
        card.transform.position = transform.position + new Vector3(0, -1, 0);

        RemoveCard(card, false, true);
    }

    public void ResetCardPositions()
    {

        Vector3 averPos = new Vector3(0, 0, 0);
        GetComponent<Rigidbody>().velocity = new Vector3();

        int halfIndex = Cards.Count / 2;

        for (int i = 0; i < Cards.Count; i++)
        {
            if (Cards[i].transform.parent == null)
            {
                Cards[i].transform.parent = gameObject.transform;
            }
        }

        if (Fanned)
        {
            if (curFannedOrien == 0)
            {
                gameObject.GetComponent<BoxCollider>().size = new Vector3(.1f + (Cards.Count * distBtwnCards), 3.7f, 2.7f + (Cards.Count * .3f));

                for (int i = 0; i < Cards.Count; i++)
                {
                    Cards[i].transform.localPosition = new Vector3((halfIndex - i) * distBtwnCards, 0, (Cards.Count - i) * 0.3f);
                    averPos += Cards[i].transform.localPosition;

                    if (IsCardFacingUp(Cards[i]))
                    {
                        Cards[i].transform.rotation = Quaternion.Euler(0, 90, 90);
                    }
                    else
                    {
                        Cards[i].transform.rotation = Quaternion.Euler(0, 90, -90);
                    }
                }
            }
            else if (curFannedOrien == 1)
            {
                gameObject.GetComponent<BoxCollider>().size = new Vector3(.1f + (Cards.Count * distBtwnCards), 3.7f + (Cards.Count * .75f), 2.7f);

                for (int i = 0; i < Cards.Count; i++)
                {
                    Cards[i].transform.localPosition = new Vector3((halfIndex - i) * distBtwnCards, (Cards.Count - i) * -0.75f, 0);
                    averPos += Cards[i].transform.localPosition;

                    if (IsCardFacingUp(Cards[i]))
                    {
                        Cards[i].transform.rotation = Quaternion.Euler(0, 90, 90);
                    }
                    else
                    {
                        Cards[i].transform.rotation = Quaternion.Euler(0, 90, -90);
                    }
                }
            }
        }
        else
        {

            if (!Input.GetMouseButton(0))
            {
                gameObject.GetComponent<BoxCollider>().size = new Vector3(.1f + (Cards.Count * distBtwnCards), 3.7f, 2.7f);
            }

            for (int i = 0; i < Cards.Count; i++)
            {
                Cards[i].transform.localPosition = new Vector3((halfIndex - i) * distBtwnCards, 0, 0);
                averPos += Cards[i].transform.localPosition;

                if (IsCardFacingUp(Cards[i]))
                {
                    Cards[i].transform.rotation = Quaternion.Euler(0, 90, 90);
                }
                else
                {
                    Cards[i].transform.rotation = Quaternion.Euler(0, 90, -90);
                }
            }

        }
        averPos /= Cards.Count;

        gameObject.GetComponent<BoxCollider>().center = averPos;
    }

    public RaycastHit GetRayCastFromMouse()
    {
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit mouseRayHit;

        Physics.Raycast(mouseRay, out mouseRayHit);

        return mouseRayHit;
    }

    private bool IsCardFacingUp(GameObject card)
    {
        if (Vector3.Dot(card.transform.right, Vector3.up) >= 0)
        {
            return true;
        }
        else
            return false;
    }

    private void DrawCard()
    {
        if (pileScript.GetCardDrawSide() == 0)
        {
            if (IsCardFacingUp(gameObject))
            {
                DropCard(Cards[0]);
            }
            else
            {
                DropCard(Cards[Cards.Count - 1]);
            }
        }
        else if (pileScript.GetCardDrawSide() == 1)
        {
            if (IsCardFacingUp(gameObject))
            {
                DropCard(Cards[Cards.Count - 1]);
            }
            else
            {
                DropCard(Cards[0]);
            }
        }
    }

    public void FanCards()
    {
        if (Fanned)
        {
            Fanned = false;
            ResetCardPositions();

            ChangeStates(States.NOT_HELD);
            gameObject.tag = "Card";

            foreach (GameObject card in Cards)
            {
                card.tag = "Card";
            }
        }
        else
        {
            ChangeStates(States.FANNED);
            Fanned = true;
            gameObject.tag = "Untagged";

            foreach (GameObject card in Cards)
            {
                card.tag = "Untagged";
            }

            int halfIndex = Cards.Count / 2;
            Vector3 averPos = new Vector3(0,0,0);

            if (pileScript.GetFannedOrien() == 0)
            {
                curFannedOrien = 0;

                gameObject.GetComponent<BoxCollider>().size = new Vector3(.1f + (Cards.Count * distBtwnCards), 3.7f, 2.7f + (Cards.Count) * 0.3f);
                
                for (int i = 0; i < Cards.Count; i++)
                {
                    Cards[i].transform.localPosition = new Vector3((halfIndex - i) * distBtwnCards, 0, (Cards.Count - i) * 0.3f);
                    averPos += Cards[i].transform.localPosition;

                    if (IsCardFacingUp(Cards[i]))
                    {
                        Cards[i].transform.rotation = Quaternion.Euler(0, 90, 90);
                    }
                    else
                    {
                        Cards[i].transform.rotation = Quaternion.Euler(0, 90, -90);
                    }
                }

                averPos /= Cards.Count;

                gameObject.GetComponent<BoxCollider>().center = averPos;
            }//end of if horizontal fanned orientation
            else if (pileScript.GetFannedOrien() == 1)
            {
                curFannedOrien = 1;

                gameObject.GetComponent<BoxCollider>().size = new Vector3(.1f + (Cards.Count * distBtwnCards), 3.7f + (Cards.Count) * .75f, 2.7f);

                for (int i = 0; i < Cards.Count; i++)
                {
                    Cards[i].transform.localPosition = new Vector3((halfIndex - i) * distBtwnCards, (Cards.Count - i) * -0.75f, 0);
                    averPos += Cards[i].transform.localPosition;

                    if (IsCardFacingUp(Cards[i]))
                    {
                        Cards[i].transform.rotation = Quaternion.Euler(0, 90, 90);
                    }
                    else
                    {
                        Cards[i].transform.rotation = Quaternion.Euler(0, 90, -90);
                    }
                }

                averPos /= Cards.Count;

                gameObject.GetComponent<BoxCollider>().center = averPos;
            }
        }
    }
}
