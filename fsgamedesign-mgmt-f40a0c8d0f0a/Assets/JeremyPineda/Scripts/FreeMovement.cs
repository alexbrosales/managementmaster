﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeMovement : MonoBehaviour {
	// Developer Name: Jeremy Pineda
	// Contribution: Movement of the card script
	// Start & End dates: 11/9/2017
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        HandleMovement();
	}


    void HandleMovement()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(0, 1, 0);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(0, -1, 0);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Rotate(0, 0, -1);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Rotate(0, 0, 1);
        }

        if(Input.GetKey(KeyCode.RightShift) && Input.GetKey(KeyCode.DownArrow))
        {
            transform.Rotate(1, 0, 0);
        }
        if (Input.GetKey(KeyCode.RightShift) && Input.GetKey(KeyCode.UpArrow))
        {
            transform.Rotate(-1, 0, 0);
        }

    }
}
