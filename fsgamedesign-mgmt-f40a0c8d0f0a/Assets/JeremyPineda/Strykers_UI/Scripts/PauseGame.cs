﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PauseGame : MonoBehaviour {
	// Developer Name: Jeremy Pineda
	// Contribution: Pause Script
	// Feature: Functionality of the the pause menu
	// Start & End dates: 11/1/2017
    public Transform Canvas;
	public string MainMenu;

   // GDN.ASCore.AvatarInput player;

  

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }

    }
	public void LoadBackToMainMenu()
	{
		SceneManager.LoadScene (MainMenu);
	}


    public void Pause()
    {
        
        
            if (Canvas.gameObject.activeInHierarchy == false)
            {
                Canvas.gameObject.SetActive(true);
                Time.timeScale = 0;
                
            }
            else
            {
                Canvas.gameObject.SetActive(false);
                Time.timeScale = 1;
                
            }
        
    }
    }
