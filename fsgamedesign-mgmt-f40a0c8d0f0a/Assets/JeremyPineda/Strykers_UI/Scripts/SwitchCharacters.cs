﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCharacters : MonoBehaviour {

    [SerializeField]
    GameObject OldPlayer;
    [SerializeField]
    GameObject NewPlayer;

    // Use this for initialization
    void Start () {
        NewPlayer.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

     void OnTriggerEnter(Collider other)
    {
        OldPlayer.SetActive(false);
            NewPlayer.SetActive(true);
        
    }
}
